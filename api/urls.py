from django.urls import path, include
from .views import BooksApiView

urlpatterns = [
    path('', BooksApiView.as_view()),
]
