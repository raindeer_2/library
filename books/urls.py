from django.urls import path, include
from .views import BooksList

urlpatterns = [
    path('', BooksList.as_view(), name='home'),
]